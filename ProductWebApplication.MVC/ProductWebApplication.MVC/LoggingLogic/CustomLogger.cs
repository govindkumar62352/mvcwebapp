﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace UserApp.LogginLogic
{
    public class CustomLogger : ActionFilterAttribute

    {
        readonly string logFileName;
        DateTime startTime;
        DateTime endTime;
        TimeSpan totalTime;

        public CustomLogger(IWebHostEnvironment environment)
        {
            logFileName = environment.ContentRootPath + @"LogFile/CustomLogger.txt";
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            startTime = DateTime.Now;
            ControllerBase controllerbase = (ControllerBase)context.Controller;
            ControllerContext controllerContext = controllerbase.ControllerContext;
            string controllerName = controllerContext.ActionDescriptor.ControllerName;
            string actionName = controllerContext.ActionDescriptor.ActionName;
            using (StreamWriter writer = File.AppendText(logFileName))

            {
                writer.Write($"StartTime::{startTime}\t ControllerName::{controllerName}\t ActionName::{actionName}");
                writer.Close();
            }

        }
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            endTime = DateTime.Now;
            totalTime = endTime - startTime;
            using (StreamWriter writer = File.AppendText(logFileName))
            {
                writer.Write($"EndTime::{endTime}\t Total Timein Seconds:: {totalTime.TotalSeconds}");
                writer.WriteLine();
                writer.Close();
            }
        }

    }
}